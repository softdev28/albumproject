/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.albumproject.albumproject.dao;

import com.thunwarat.albumproject.albumproject.model.ReportSale;
import com.thunwarat.albumproject.helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ACER
 */
public class SaleDao {

    public List<ReportSale> getDayReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\", InvoiceDate) as period,SUM(Total) as Total FROM invoices\n"
                + "LEFT JOIN invoice_items\n"
                + "On invoices.InvoiceId = invoice_items.InvoiceId\n"
                + "GROUP BY period\n"
                + "ORDER BY period Desc\n"
                + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getMonthReport(int year) {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m-%d\", InvoiceDate) as period,SUM(Total) as Total FROM invoices\n"
                + "LEFT JOIN invoice_items\n"
                + "On invoices.InvoiceId = invoice_items.InvoiceId\n"
                + "WHERE strftime(\"%Y\", InvoiceDate)=\"" + year +"\"\n"
                + "GROUP BY period \n"
                + "ORDER BY period Desc\n"
                + "\n"
                + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
